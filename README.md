dependencias usadas, versiones, descripcion

TeShoes

Es una pagina Ecommerce con formato SPA de Zapatillas, simulando la seleccion del producto y su proceso de compra hasta la creacion de la orden para despues guardarlo en una "base de datos" externa

Dependencias utilizadas y sus respectivas versiones:
    "bootstrap": "^5.2.3",
    "firebase": "^9.17.2",
    "react": "^18.2.0",
    "react-bootstrap": "^2.7.0",
    "react-dom": "^18.2.0",
    "react-router-dom": "^6.8.1"



Bootstrap y React Bootstrap:
Utilizada para agregar estilos de manera sencilla mediante clases a la pagina.

FireBase:
Brinda una base de datos noSQL facil de conectar con nuestra aplicacion

React Router Dom:
Simplifica la navegacion de la pagina mediante links reemplazando la necesidad de crear varios HTMLs y brindando posibilidad de redirección, interpretacion de variables en url y más utilidades.

demo: https://main--shimmering-faun-d49ce4.netlify.app/
