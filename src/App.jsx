import { useState } from 'react'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';

import NavBar from './Components/NavBar/NavBar'
import ItemListContainer from './Components/ItemListContainer/ItemListContainer';
import ItemDetailContainer from './Components/ItemDetailContainer/ItemDetailContainer';
import { BrowserRouter, Navigate, Route, Routes  } from 'react-router-dom';
import { CartContextProvider } from './Context/CartContext';
import CartContainer from './Components/CartContainer/CartContainer';
import OrderSuccess from './Components/OrderSuccess/OrderSuccess';
import Error from './Components/Error/Error';

function App() {
  const [count, setCount] = useState(0)

  return (
    <BrowserRouter >

              <CartContextProvider>
                <NavBar/>
                    <div>
                        <Routes>
                            <Route  path='/' element={ <ItemListContainer/> } />
                            <Route  path='/categoria/:idCategoria' element={ <ItemListContainer/> } />
                            <Route  path='/articulo/:idProduct' element={ <ItemDetailContainer /> } />
                            <Route  path='/cart' element={ <CartContainer />  } />               
                            <Route path='/success/:orderID' element={<OrderSuccess/>}></Route>
                            <Route  path='/404Error' element={ <Error/>  } />               

                            <Route path='*' element={ <Navigate to='/404Error' /> } />
                        </Routes>
                    </div>
              </CartContextProvider>

    </BrowserRouter>

  )
}

export default App
