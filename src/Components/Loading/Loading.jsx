import React from 'react'

const Loading = () => {
  return (
    <h2 style={{
        textAlign: 'center',
        marginTop: '20%',
        fontweight: 'bolder',
      }}>Cargando...</h2>   )
}

export default Loading