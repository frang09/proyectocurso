import React from 'react'

import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { doc, getDoc, getFirestore } from "firebase/firestore"

import ItemDetail from '../ItemDetail/ItemDetail'
import Loading from '../Loading/Loading'


const ItemDetailContainer = () => {
    const [ product, setProduct ] =  useState({})
    const { idProduct } = useParams()
    const [ loading, setLoading ] = useState(true)

    useEffect(()=>{    
        setLoading(true)   
        const db = getFirestore() 
        const query = doc(db, 'products', idProduct)
        getDoc(query)
        .then(resp => setProduct( { id: resp.id, ...resp.data() } ))
        .catch(err => console.error(err))
        .finally(() => setLoading(false))
     }, [])
    
    return (

      loading 
      ? 
      <Loading/>
      : 
      <div>
          <ItemDetail product={product} />
      </div>
    )
  }

export default ItemDetailContainer