import { memo } from "react"
import { Link } from "react-router-dom"
import './Item.css'

const Item =  memo(  ({product}) => {

    return (
      <div className='card-shop m-2' >

                    <Link to={`/articulo/${product.id}`} style={{textDecoration: 'none'}}>
                      <div>
                        <img src={product.img} alt='imagen' className="w-75 ms-5"/>

                        <div className="text-center card-text" >
                         ${product.price}<br />
                         {product.name}
                        </div>

                      </div>
                    </Link>
  
                  </div>
    )
  }

)

export default Item