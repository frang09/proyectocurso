import React from 'react'
import { useEffect, useState } from "react"
import { Link, useParams } from "react-router-dom"
import ItemList from '../ItemList/ItemList'
import Loading from '../Loading/Loading'
import "./ItemListContainer.css" 
import { collection, getDocs, getFirestore, query, where } from 'firebase/firestore'


export const ItemListContainer = () => { 
  const [ products, setProducts ] = useState([])
  const [ loading, setLoading ] = useState(true)
  const [ order, setOrder ] = useState()

  const { idCategoria } = useParams()


  useEffect(()=>{
    setLoading(true)

    const db = getFirestore() 
    const queryCollections = collection(db, 'products')
    const queryFilter = idCategoria ? query(queryCollections, where('cat','==', idCategoria) ) : queryCollections 

    getDocs(queryFilter)
    .then(resp => setProducts( resp.docs.map(product => ({ id: product.id, ...product.data() } ) )))
    .catch(err => console.error(err))
    .finally(() => setLoading(false))    
  }, [idCategoria])


  return (
    
         loading 
          ? 
          <Loading/>
          : 
          <>
          <div>
              <div style={{
                width: '50%',
                padding: '50px',
                marginLeft: '25%',
              }} className= 'd-flex justify-content-around'>
                
              </div>
              <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                paddingTop: '30px',
                width: '80%',
                marginLeft: '10%',
                backgroundColor: '#fff'

              }} >

                <ItemList products={products} />                    

              </div>
            </div>
          </>
          
  )
}

export default ItemListContainer